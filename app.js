let titleInput = document.querySelector('#title');
let post = document.querySelector('#posts');
let bodyInput = document.querySelector('#body');
let idInput = document.querySelector('#id');
let postSubmit = document.querySelector('.post-submit');

document.addEventListener('DOMContentLoaded', getPosts);
document.querySelector('.post-submit').addEventListener('click', submitPost);
document.querySelector('#posts').addEventListener('click', deletePost);
document.querySelector('#posts').addEventListener('click', editPost);

function getPosts() {
	fetch('http://localhost:3000/posts')
		.then((response) => response.json())
		.then((posts) => {
			console.log(posts);
			showPosts(posts);
		})
		.catch((err) => console.log(err));
}

function showPosts(posts) {
	console.log(posts);
	let output = '';
	posts.forEach((post) => {
		output += `
		  <div class="card mb-3">
			<div class="card-body">
			  <h4 class="card-title">${post.title}</h4>
			  <p class="card-text">${post.body}</p>
			  <a href="#" class="edit card-link" data-id="${post.id}">
				<i class="fa fa-pencil"></i>
			  </a>
			  <a href="#" class="delete card-link" data-id="${post.id}">
			  <i class="fa fa-remove"></i>
			</a>
			</div>
		  </div>
		`;
	});

	post.innerHTML = output;
}

// Submit Post
function submitPost() {
	const title = document.querySelector('#title').value;
	const body = document.querySelector('#body').value;
	const id = document.querySelector('#id').value;

	const data = {
		title,
		body,
	};

	// Validate input
	if (title !== '' || body !== '') {
		if (id === '') {
			// Create Post
			fetch('http://localhost:3000/posts', {
				method: 'POST',
				body: JSON.stringify(data),
				headers: {
					'Content-type': 'application/json; charset=UTF-8',
				},
			})
				.then(() => {
					getPosts();
				})
				.catch((err) => console.log(err));
		}
	}
}

function editPost(e) {
	if (e.target.parentElement.classList.contains('edit')) {
		const id = e.target.parentElement.dataset.id;

		let cardTitle =
			e.target.parentElement.previousElementSibling.previousElementSibling;
		let cardText = e.target.parentElement.previousElementSibling;

		if (confirm('Sure to edit?')) {
			cardTitle.style.color = 'red';

			cardText.style.color = 'red';
			cardText.style.fontSize = 26;
			cardText.style.fontWeight = 600;

			fetch(`http://localhost:3000/posts/${id}`, {
				method: 'PUT',
				body: JSON.stringify({
					title: `Complete: ${cardTitle.textContent}`,
					body: `Complete: ${cardText.textContent}`,
				}),
				headers: {
					'Content-type': 'application/json; charset=UTF-8',
				},
			})
				.then(() => {
					getPosts();
				})

				.catch((err) => console.log(err));
		}
	}
	e.preventDefault();
}

// Delete Post
function deletePost(e) {
	if (e.target.parentElement.classList.contains('delete')) {
		const id = e.target.parentElement.dataset.id;
		if (confirm('Are you sure?')) {
			fetch(`http://localhost:3000/posts/${id}`, {
				method: 'DELETE',
			})
				.then(() => {
					getPosts();
				})
				.catch((err) => console.log(err));
		}
	}
	e.preventDefault();
}
